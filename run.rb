#!/usr/bin/env ruby

require 'mechanize'
require 'json'
require 'rest-client'

WEEKDAY_IN_JAPANESE = ['日', '月', '火', '水', '木', '金', '土']

agent = Mechanize.new()

# なぜか404コードを返してくる
menu_list_url = 'http://www.tamagoya.co.jp/menu_list/'

html = nil

# 404ページにメニューが記載されているのでrescueで拾い出す
begin
  result = agent.get(menu_list_url)
rescue Mechanize::ResponseCodeError => e
  html = e.page.body
end

# めんどいのでNokogiriでスクレイピングする
doc = Nokogiri::HTML.parse(html, nil, nil)
doc_menus = doc.xpath("//div[@id='weeklymenuList']").css('.item')

menus = Array.new
doc_menus.each do |doc_menu|
  menu = Hash.new

  menu['title'] = doc_menu.css('.title').text.gsub(/\r\n/, " ")
  
  menu['day']   = doc_menu.css('.day').text

  weekday = WEEKDAY_IN_JAPANESE.index(menu['day'][/\(.*?\)/][1])
  day = menu['day'].gsub(/日.*/, "").to_i
  today = Date.today()

  menu['date'] = if (today - today.wday + weekday).day == day 
    today - today.wday + weekday
  elsif (today - today.wday + weekday).day < day
    today - today.wday + 7 + weekday
  else
    today - today.wday - 7 + weekday
  end

  menu['foods'] = doc_menu.css('.text').text.split("\n").map do |food|
    food.gsub(/\r\n/, "").gsub(/\r/, "")
  end

  menu['rice_calorie_kcal'] = doc_menu.css('.option').text.split("\n").find do |calorie|
    calorie.include?('ライス')
  end.split('カロリー')[-1].gsub(/\r/, "").gsub(/kcal/, "").to_i

  menu['foods_calorie_kcal'] = doc_menu.css('.option').text.split("\n").find do |calorie|
    calorie.include?('おかず')
  end.split('カロリー')[-1].split('／')[0].gsub(/kcal/, "").to_i

  menu['salinity_gram'] = doc_menu.css('.option').text.split("\n").find do |calorie|
    calorie.include?('おかず')
  end.split('カロリー')[-1].split('／')[-1].gsub(/^塩分/, "").gsub(/g$/, "").to_f

  menu['allergies'] = doc_menu.css('.allergy').xpath('li').find_all do |allergen| 
    !allergen.values.include?('silence')
  end.map do |allergen|
    allergen.text
  end

  menus.push(menu)
end

require 'pp'

pp menus

todays_menu = menus.find do |menu|
  menu['day'] == Date.today().strftime("%d日(#{WEEKDAY_IN_JAPANESE[Date.today().wday]})")
end

todays_title = '  ' + todays_menu['title']
todays_foods_list = todays_menu['foods'].map do |food|
  '  - ' + food
end.join("\n")

todays_menu_description = <<-"EOS"
本日のたまごや情報 (参照元: #{menu_list_url})

弁当タイトル
#{todays_title}

おかず一覧
#{todays_foods_list}
EOS

puts todays_menu_description